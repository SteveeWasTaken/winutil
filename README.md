# Windows Multi-Utilities Tool

### Welcome to our GitHub!
#### If you would like to know more about our project, please continue reading.
##### [More info about the Developer here](https://my.bio/steveewastaken)

The Windows Multi Utilities (or WinUtil for short) is a small project made to make things easier for those who don't know a lot about *command prompt, coding, and stuff like that.* Of course, this thing can't do magic, but it can do lots of stuff that users might want to shorten, or make easier! 

For example, ***quickly opening a folder that is nested under lots of other folders, or maybe even restart their file explorer if it's frozen.***

![The UI can be seen here. ](https://raw.githubusercontent.com/SteveYT77/winutil/main/winutil-media/ui-1.7.png "The look of the UI.")

This, of course, is still a work in progress. We hope that soon, this becomes a bigger program than what we currently are, and that we can help tons of users that might need.

Thanks for using WinUtil!

[This readme was last updated on: 23/11/2021]
